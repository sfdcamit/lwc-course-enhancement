/**
 * @description       : 
 * @author            : Amit Singh
 * @group             : 
 * @last modified on  : 12-27-2020
 * @last modified by  : Amit Singh
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   12-26-2020   Amit Singh   Initial Version
**/
import { LightningElement, wire } from "lwc";

import {
    subscribe,
    unsubscribe,
    APPLICATION_SCOPE,
    MessageContext,
    releaseMessageContext
} from "lightning/messageService";

import SAMPLEMC from "@salesforce/messageChannel/lwcMessage__c";
import SampleMessage from "@salesforce/messageChannel/SampleMessage__c";


export default class SubscribeComponent extends LightningElement {

    @wire(MessageContext) messageContext;

    subscription = null;
    receivedMessage;
    isDisabled = false;
    isDisabledUnsb = true;

    subscribeMC() {
        this.isDisabled = true;
        this.isDisabledUnsb = false;
        if (this.subscription) {
            return;
        }
        this.subscription = subscribe( this.messageContext, SAMPLEMC,
        message => {
            this.handleMessage(message);
        },
        { scope: APPLICATION_SCOPE }
        );  
    }

    unsubscribeMC() {
        unsubscribe(this.subscription);
        this.subscription = undefined;
        this.isDisabled = false;
        this.isDisabledUnsb = true;
    }

    handleMessage(message) {
        window.console.log('recieved the message ');
        this.receivedMessage = message
        ? JSON.stringify(message, null, "\t")
        : "no message payload";
    }

    disconnectedCallback(){
        releaseMessageContext(this.messageContext);
    }
}