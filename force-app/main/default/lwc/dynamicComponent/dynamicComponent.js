/**
 * @description       : 
 * @author            : Amit Singh
 * @group             : 
 * @last modified on  : 12-25-2020
 * @last modified by  : Amit Singh
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   12-25-2020   Amit Singh   Initial Version
**/
import { LightningElement } from 'lwc';

import firstTemplate from './dynamicComponent.html';
import templateOne   from './templateOne.html';
import templateTwo   from './templateTwo.html';


export default class DynamicComponent extends LightningElement {

    templateOne = false;
    templateTwo = false;

    render(){

        if( !this.templateOne && !this.templateTwo){
            return firstTemplate;
        }else if(this.templateOne && !this.templateTwo){
            return templateOne;
        }else if(this.templateTwo && !this.templateOne){
            return templateTwo;
        }
        /*
            templateOne  => false = firstTemplate
            templateOne => true  = templateOne
        */
    }

    handleClick(){
        this.templateOne = this.templateOne === true ? false : true;
    }
    handleClickTempl2(){
        this.templateOne = false;
        this.templateTwo = true;
    }

    stchTmpl(){
        this.templateOne = false;
        this.templateTwo = false;
    }
}