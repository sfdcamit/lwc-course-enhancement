/**
 * @description       : 
 * @author            : Amit Singh
 * @group             : 
 * @last modified on  : 12-27-2020
 * @last modified by  : Amit Singh
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   12-26-2020   Amit Singh   Initial Version
**/
import { LightningElement, wire } from 'lwc';

import { publish, MessageContext } from 'lightning/messageService';
import SAMPLEMC from '@salesforce/messageChannel/lwcMessage__c';

export default class PublisherComponent extends LightningElement {

    @wire(MessageContext)
    messageContext;

    handleClick() {
        window.console.log('pubslising the message...')
        const message = {
            recordId: '001xx000003NGSFAA4',
            message : "This is simple message from LWC",
            source: "LWC",
            recordData: {accountName: 'Burlington Textiles Corp of America', accountSite : 'sfdcpanther.com' }
        };
        publish(this.messageContext, SAMPLEMC, message);
        window.console.log('published the message...')
    }
}