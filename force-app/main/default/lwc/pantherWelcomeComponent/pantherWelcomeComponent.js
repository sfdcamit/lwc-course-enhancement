/**
 * @description       : 
 * @author            : Amit Singh
 * @group             : 
 * @last modified on  : 12-25-2020
 * @last modified by  : Amit Singh
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   12-25-2020   Amit Singh   Initial Version
**/
import { LightningElement } from 'lwc';

export default class PantherWelcomeComponent extends LightningElement {

    name = 'Amit';

    studentRecord = {
        name : 'Amit Singh',
        email : '',
        phone : '',
        address : ''
    };

    studentRecords = [{
        name : 'Anuj Singh',
        email : '',
        phone : '',
        address : ''
    }];

    handleClick(){
        this.name = 'WoooooHoooo this worked'
        
        this.studentRecord.name = 'Kim Jong';
        
        this.studentRecords.push({
            name : 'Jhon Doe',
            email : '',
            phone : '',
            address : ''
        });
    }
}