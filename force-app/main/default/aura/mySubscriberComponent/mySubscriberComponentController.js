/**
 * @description       : 
 * @author            : Amit Singh
 * @group             : 
 * @last modified on  : 12-27-2020
 * @last modified by  : Amit Singh
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   12-27-2020   Amit Singh   Initial Version
**/

({
    handleMessage: function(cmp, event, helper) {
      // Read the message argument to get the values in the message payload
        if (event != null && event.getParams() != null) {
            let params = event.getParams();
            cmp.set("v.recordValue", JSON.stringify(params, null, "\t"));
        }
    }
});