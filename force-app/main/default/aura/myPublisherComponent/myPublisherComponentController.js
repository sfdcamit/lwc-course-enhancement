/**
 * @description       : 
 * @author            : Amit Singh
 * @group             : 
 * @last modified on  : 12-27-2020
 * @last modified by  : Amit Singh
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   12-27-2020   Amit Singh   Initial Version
**/
({
    handleClick : function(component, event, helper) {
        const message = {
            recordId: '001xx000003NGSFAA4',
            message : "This is simple message from Aura Component",
            source: "AURA Component",
            recordData: {accountName: 'Burlington Textiles Corp of America', accountSite : 'sfdcpanther.com' }
        };

        component.find('lwcMessage').publish(message);
    }
})
